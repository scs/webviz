export function updateState(state, keys, value) {
    // A function to update a nested state, where the key is given as a list of 
    // object keys or array indices.
    var newState;
    if (Object.prototype.toString.call(state) == '[object Array]') {
        newState = [...state];
    } else if (Object.prototype.toString.call(state) == '[object Object]') {
        newState = {...state};
    }

    let key = keys[0];
    let remainingKeys = keys.slice(1);
    if (key !== undefined) {
        newState[key] = updateState(newState[key], remainingKeys, value)
        return newState;
    } else {
        return value;
    }

}

export function getNestedData(container, key) {
    // Key may be something like "data.messages.0.name", i.e. a mix
    // of dictionary keys and list indices, or directly a list of keys ["data","messages",0,"name"]
    // This function will try to recursively apply the key to the container
    // and return the final value, if present, otherwise undefined
    let keyArr = []
    if (typeof(key) === "string") {
        keyArr = key.split(".");
    } else {
        keyArr = key;
    }
    
    let [head, ...tail] = keyArr;
    if (tail.length === 0) {
        return container[head]
    } else {
        return getNestedData(container[head], tail)
    }


}