import React, { PureComponent } from 'react';


export default class ListSelection extends PureComponent {

    render () {
        var { name, options1, onChange1, selected1, options2, onChange2, selected2 } = this.props;
        var optionList = [];
        for (var i=0; i< options1.length; i++) {
            optionList.push(
                <option key={i} value={options1[i]} selected={options1[i]===selected1}>
                    {options1[i]}
                </option>
            )
        }

        if (options2) {
            var option2List = [];

            options2 = options2.sort((a, b) => a.localeCompare(b, 'en', { numeric: true }))
            for (var i=0; i< options2.length; i++) {
                option2List.push(
                    <option key={i} value={options2[i]} selected={options2[i]===selected2}>
                        {options2[i]}
                    </option>
                )
            }
        }

        return (
            <div className="conList-item">
                {name}: 
                <select id={name+"Sel1"} onChange={onChange1} >
                    {optionList}
                </select>
                {options2 ? <select id={name+"Sel2"} onChange={onChange2}>
                    {option2List}
                </select> : ""
                }
            </div>
        )
    }

}