import React, { Component } from 'react';

import CanvasGridworld from "./gridworld"
import CustomSlider from "./slider"
import { BarPlot } from './plots';
import {VerticalBarSeries, DiscreteColorLegend} from "react-vis"; 
import ListSelection from './listSelection';
import continuousColorLegend from 'react-vis/dist/legends/continuous-color-legend';
import CheckboxList from './checkboxList';

class StudyReview extends Component {

    constructor (props, context) {
        super(props, context)

        let startMap = "";
        for (var key in props.maps) {
            startMap = key;
            break;
        }

        this.state = {
            curMap: startMap,
            collapse: false,
            stepNr: 0,
            normalize: false,
            dataSrc: {}
        }

        this.onChangeCollapse = this.onChangeCollapse.bind(this);
        this.onChangeDataSource = this.onChangeDataSource.bind(this);
        this.onSliderChange = this.onSliderChange.bind(this);
        this.onMapSelect = this.onMapSelect.bind(this);
        this.prevQP = this.prevQP.bind(this);
        this.nextQP = this.nextQP.bind(this);

        this.curQP = null;
    }

    onChangeCollapse() {
        this.setState({
            normalize: !this.state.normalize
        })
    }

    onSliderChange(value) {
        this.setState({stepNr: value})

    }
    
    onMapSelect(e) {
        this.setState({curMap: e.target.value})
    }

    prevQP() {
        let answersC = this.props.answers["Sampling"]
        let curQP = this.state.stepNr;
        while (curQP > 0) {
            curQP -= 1;
            if (curQP in answersC[this.state.curMap]) {
                this.setState({stepNr: curQP});
                break;
            }
        }
    }

    nextQP() {
        let answersC = this.props.answers["Sampling"]
        let curQP = this.state.stepNr;
        while (curQP < this.props.trajs[this.state.curMap].length) {
            curQP += 1;
            if (curQP in answersC[this.state.curMap]) {
                this.setState({stepNr: curQP});
                break;
            }
        }
    }

    onChangeDataSource(name, newState) {
        // let dataSource = e.target.id.slice(0,-5);
        let newDataSrc = Object.assign({}, this.state.dataSrc);
        newDataSrc[name] = newState;

        this.setState({
            dataSrc: newDataSrc
        })
    }

    render() {
        let {maps, trajs, answers, width, height } = this.props;
        const colors = {"C1,C2;C1,C2": "blue",
                        "C1,C2;C2,C1": "orange",
                        "C2,C1;C1,C2": "green",
                        "C2,C1;C2,C1": "purple",
                        "Group1": "orange",
                        "Group2": "blue",
                        "Group2BE": "lightblue",
                        "twg": "green",
                        "tw": "purple",
                        "tw2": "lightblue" ,
                        "Sampling":"red",
                        "Switching": "black"
                        }
        let curMap = this.state.curMap;
        let map = maps[curMap];
        let traj = trajs[curMap];

        let curStep = this.state.stepNr;
        let pos = trajs[curMap][curStep];

        let bars = [];
        let barsControlled = [];

        let answersC_Controlled = answers["collapsed_control"];
        let answersI_Controlled = answers["individual_control"];

        const answerOrder = ["R", "B", "O", "Y", "U", "Yes", "No"];

        this.curQP = curStep;

        var normalize = this.state.normalize || this.state.dataSrc["twg"] || this.state.dataSrc["tw"] || this.state.dataSrc["Switching"];

        for (var dataSrc in this.state.dataSrc) {
            if (this.state.dataSrc[dataSrc]) {
                let vals = answers[dataSrc]
                let data = [];
                if (vals[curMap][this.curQP]) {

                    var norm = 0;
                    var normBelief = 0;
                    if (normalize) {
                        for (var i=0;i<answerOrder.length; i++) {
                            if (vals[curMap][this.curQP][answerOrder[i]]) {
                                if (answerOrder[i] === "Yes" || answerOrder[i] === "No") {
                                    normBelief += vals[curMap][this.curQP][answerOrder[i]];
                                } else {
                                    norm += vals[curMap][this.curQP][answerOrder[i]];
                                }
                            }
                        }
                    } else {
                        norm = 1;
                        normBelief = 1;
                    }
                    for (var i=0;i<answerOrder.length; i++) {
                        var answer = answerOrder[i];
                    
                        if (answer in vals[curMap][this.curQP]) {
                            if (answer === "Yes" || answer === "No") {
                                data.push({"x": answer, "y": vals[curMap][this.curQP][answer]/normBelief})
                            } else {
                                data.push({"x": answer, "y": vals[curMap][this.curQP][answer]/norm})
                            }

                        }
                    }
                }

                if (data.length > 0) {
                    bars.push(<VerticalBarSeries key={dataSrc} 
                        data={data} 
                        color={colors[dataSrc]} />)
                }
            }
        }

        
        var data = [];
        if (answersC_Controlled[curMap][this.curQP]) {
            for (var i=0;i<answerOrder.length; i++) {
                var answer = answerOrder[i];
                if (answer in answersC_Controlled[curMap][this.curQP]) {
                    data.push({"x": answer, "y": answersC_Controlled[curMap][this.curQP][answer]})
                }
            }
        }

        if (data.length > 0) {
            barsControlled.push(<VerticalBarSeries key={"collapsed"} 
                data={data} 
                color={"red"} />)
        }


        let barsIControlled = [];
        for (var variant in answersI_Controlled) {
            console.log("variant2: ", variant)
            if (variant === "null") {
                continue;
            }
            let data = [];
            if (answersI_Controlled[variant][curMap][this.curQP]) {
                for (var i=0;i<answerOrder.length; i++) {
                    var answer = answerOrder[i];
                    if (answer in answersI_Controlled[variant][curMap][this.curQP]) {
                        data.push({"x": answer, "y": answersI_Controlled[variant][curMap][this.curQP][answer]})
                    }
                }
            }
            if (data.length > 0) {
                barsIControlled.push(<VerticalBarSeries key={variant} 
                    data={data} 
                    color={colors[variant]} />)
            }
        }


        return (
            <div>
                <div className="condition">{"Study Results"}</div>
                <div className={"slider-controls"}>
                <CanvasGridworld width={width/2} height={height} 
                                map={map} bgname={"bg"}
                                showTargets={true}
                                showTrueColor={true}
                                showPath={true}
                                traj={traj.slice(0, curStep+1)}
                                pos={pos}
                                />
                    <div>
                        {/* <h3>Controlled:</h3> */}
                        {/* <div className={"slider-controls"}>
                            {barsIControlled.length > 0 ? <BarPlot width={width/4} height={height/4} bars={barsControlled}/> : ""}
                            {barsIControlled.length > 0 ? <BarPlot width={width/4} height={height/4} bars={barsIControlled}/> : ""}
                        </div> */}
                        {bars.length > 0 ? <DiscreteColorLegend
                        orientation="horizontal"
                        height={80}
                        items={[
                                {"title": "Group 1", "color": colors["Group1"]},
                                {"title": "Group 2", "color": colors["Group2"]},
                                {"title": "Group 2BE", "color": colors["Study3BE"]},
                                {"title": "Sampling", "color": colors["Sampling"]},
                                {"title": "True Belief", "color": colors["twg"]},
                                {"title": "Unknown", "color": colors["tw"]},
                                {"title": "Switching", "color": colors["Switching"]},
                                ]}
                        />: ""}
                        <div className={"slider-controls"}>
                            {bars.length > 0 ? <BarPlot width={width/2} height={height/1.8} bars={bars} yDomain={normalize ? [0,1]: null}/> : ""}
                        </div>
                    </div>
                </div>
                <div className={"flex"}>
                    <div>
                        <ListSelection name={"Select Map"} options1={Object.keys(answers["Sampling"])} onChange1={this.onMapSelect} selected1={Object.keys(answers["Sampling"])[0]} />
                    </div>
                    <div>
                        <button onClick={this.prevQP} >{"<"}</button>
                        <button onClick={this.nextQP} >{">"}</button>
                    </div>
                    <div>
                        Normalize:
                        <input type="checkbox" defaultChecked={this.state.normalize} checked={this.state.normalize} onChange={this.onChangeCollapse} />
                    </div>
                </div>
                <CheckboxList boxes={["Group1","Group2","Group2BE", "Sampling","twg","tw","Switching","tw2"]} callback={this.onChangeDataSource} />
                <CustomSlider value={curStep} min={0} max={traj.length-1} onSliderChange={this.onSliderChange}/>
                {/* {bars.length > 0 ? <DiscreteColorLegend
                        orientation="horizontal"
                        height={80}
                        items={[
                                {"title": "Study2", "color": colors["Study2"]},
                                {"title": "Study3", "color": colors["Study3"]},
                                {"title": "Study3BE", "color": colors["Study3BE"]},
                                {"title": "Sampling", "color": colors["Sampling"]},
                                {"title": "twg", "color": colors["twg"]},
                                {"title": "tw", "color": colors["tw"]},
                                {"title": "tw2", "color": colors["tw2"]},
                                ]}
                        />: ""}
                <div className={"slider-controls"}>
                    {bars.length > 0 ? <BarPlot width={width} height={height/5} bars={bars} yDomain={normalize ? [0,1]: null}/> : ""}
                </div> */}
                
            </div>
        )
    }

}

export default StudyReview;