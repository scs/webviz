import React, { PureComponent } from 'react';
import { randomFill } from 'crypto';

class CanvasGridworld extends PureComponent {

    constructor (props, context) {
        super(props, context);

        this.onMouseMove = this.onMouseMove.bind(this);
    }

    componentDidMount() {
        this.updateCanvas();
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
            this.updateCanvas();
    }

    onMouseMove(e) {
        // console.log("mouse at : ", e)//e.screenX, e.screenY)
        let canvas = this.refs[this.props.bgname];
        let r = canvas.getBoundingClientRect();
        var mouseX = e.clientX - r.left;
        var mouseY = e.clientY - r.top;
        // console.log("mouse at: ", mouseX, mouseY)
    }

    renderMap() {
        let map = this.props.map.map;
        let canvas = this.refs[this.props.bgname];
        // let canvas = document.getElementById(this.props.name);
        let context = canvas.getContext('2d');

        // var canvasMaxWidth = window.innerWidth * scaling;
        // var canvasMaxHeight = window.innerHeight * scaling;

        let canvasMaxWidth = canvas.width;
        let canvasMaxHeight = canvas.height;

        var tileSize = Math.min(Math.floor(canvasMaxWidth/map[0].length), Math.floor(canvasMaxHeight/map.length));
        canvas.width = tileSize*map[0].length;
        canvas.height = tileSize*map.length;
        context.clearRect(0, 0, canvas.width, canvas.height);            

        let renderTile = this.renderTile;

        const showTrueTarget = this.props.showTrueTarget;
        const showBeliefSymbols = this.props.showBeliefSymbols;
        const goalPos = this.props.map.goalPos;
        const targets = this.props.map.targets;
        const goalBeliefs = this.props.beliefs ? this.props.beliefs.goal: null;
        const worldBelief = this.props.beliefs ? this.props.beliefs.world : null;

        const showVisibles = this.props.showVisibles;
        const showBeliefedVision = this.props.showBeliefedVision;
        const visibles = this.props.visibles;


        if (visibles && (showVisibles || (showBeliefedVision && worldBelief === "FreeSpace"))) {
            // Render everything black and only render visible tiles!
            context.fillStyle = "black";
            context.fillRect(0,0, canvas.width, canvas.height);
            visibles.forEach(function(el) {
                let tile = Object.assign({}, map[el[0]][el[1]]);
                renderTile(context, tile, tileSize, el[1], el[0]);
            })
        } else {
            map.forEach(function(row,i, arr) {
                row.forEach(function(tile, j, row) { 
                        tile = Object.assign({}, tile);
                        if ((showTrueTarget) && i === goalPos[0] && j === goalPos[1]) {
                            
                            tile.color = "green";
                            tile.symbol = "T";
                        }   
                        renderTile(context, tile, tileSize, j, i);
                })
            })

        }

        if (showBeliefSymbols && goalBeliefs) {
            for (var i=0; i<targets.length; i++) {
                let pos = targets[i].key;
                let tile = Object.assign({}, map[pos[0]][pos[1]]);
                tile.symbol = goalBeliefs[i];
                tile.color = "lightblue";
                renderTile(context, tile, tileSize, pos[1], pos[0]);
            }
        }

        if (goalBeliefs) {
            targets.forEach( (tile,i) => {
                let pos = tile.key;
                let symbol = tile.val.symbol;
                let beliefSymbol = goalBeliefs[i];
                if (beliefSymbol == this.props.beliefs.desire) {
                    let tileContent = Object.assign({}, tile.val);
                    tileContent.color = "lightgreen";
                    tileContent.symbol = beliefSymbol;
                    renderTile(context, tileContent, tileSize, pos[1], pos[0]);
                }
            });
        }

        if (this.props.showTargets) {
            targets.forEach( tile => {
                let pos = tile.key;
                let symbold = tile.val.symbol;
                let tileContent = Object.assign({}, tile.val);

                if (!this.props.showTrueColor) {
                    var hideTile = true;
                    if (visibles) {
                        for (var i in visibles) {
                            if (visibles[i][0] == pos[0] && visibles[i][1] == pos[1]) {
                                hideTile = false;
                            }
                        }
                    } 
                    if (hideTile) {
                        console.log("render green")
                        tileContent.color = "green";
                        tileContent.symbol = "";
                    }
                }
                if (showTrueTarget && pos[0] == goalPos[0] && pos[1] == goalPos[1]) {
                    tileContent.symbol = "T";
                }
                renderTile(context, tileContent, tileSize, pos[1], pos[0]);
            })
        }

        this.tileSize = tileSize;
    }

    renderTile(context, tile, tileSize, posX, posY) {
        context.fillStyle = tile.color === "" ? "rgba(255,255,255,1)" : tile.color;
        context.fillRect(posX * tileSize, posY* tileSize, tileSize, tileSize);

        context.font = 0.8*tileSize +"px Ariel";
        context.fillStyle = "black";
        context.fillText(tile.symbol, posX*tileSize+0.2*tileSize, posY*tileSize+0.8*tileSize);
    }

    renderAgent() {
        // tileSize = sizeInfo[0];
        // canvas.width = sizeInfo[1];
        // canvas.height = sizeInfo[2];
        let tileSize = this.tileSize;
        let posX = this.props.pos[1];
        let posY = this.props.pos[0];
        const canvas = this.refs[this.props.bgname];
        var context = canvas.getContext("2d");
        context.lineWidth = 1;
        context.strokeStyle = "black";
        // context.clearRect(0, 0, canvas.width, canvas.height);
        context.beginPath();
        let centerX = posX*tileSize + tileSize/2;
        let centerY = posY*tileSize + tileSize/2;
        context.arc(centerX, centerY, 
                    tileSize*0.4, 0, 2*Math.PI);
        context.fillStyle = "yellow";
        context.fill();
        context.stroke();
        
        let eyeSize = tileSize/16;
        //Draw left eye
        context.fillStyle = "black";
        context.beginPath();
        context.arc(centerX-tileSize/7, centerY-tileSize/12, eyeSize, 0, 2*Math.PI);
        context.fill();
        context.stroke();
        
        //Draw right eye
        context.beginPath();
        context.arc(centerX+tileSize/7, centerY-tileSize/12, eyeSize, 0, 2*Math.PI);
        context.fill();
        context.stroke();
        
        //Draw mouth
        context.beginPath();
        context.arc(centerX, centerY, tileSize/5, Math.PI*5/6, Math.PI*1/6, true);
        context.stroke();
        
    }

    renderPath() {
        const canvas = this.refs[this.props.bgname];
        let tileSize = this.tileSize;
        let traj = this.props.traj;
        
        var context = canvas.getContext("2d");         
        context.strokeStyle = "red";
        context.beginPath();
        traj.forEach(function(pos, i, traj) {
            
            let centerX = pos[1]*tileSize + tileSize/2;
            let centerY = pos[0]*tileSize + tileSize/2;
            if (i == 0) {
                context.moveTo(centerX, centerY);   
            } else {
                context.lineTo(centerX,centerY);
            }
            
        })
        context.stroke();       
    }


    getColor(i, maxColors) {
        var hue = i*(360 / maxColors);
        var sat = 60 + Math.random() * 10;
        var light = 50 + Math.random() * 10;

        return `hsl(${hue}, ${sat}%, ${light}%)`
    }

    renderPredictions() {
        
    }

    renderAggregates() {
        const canvas = this.refs[this.props.bgname];
        let tileSize = this.tileSize;
        let aggregates = this.props.aggregates;
        
        var context = canvas.getContext("2d");       
        // context.lineWidth=2;  

        var posThick = {};
        var visited = {};
        for (var i=0; i< aggregates.length; i++) {
            visited= {};
            var trajObj = aggregates[i];
            var name = Object.keys(trajObj)[0];
            var traj = trajObj[name];
            traj.forEach(function(pos, j) {
                var posString = JSON.stringify(pos);
                
                if (!visited[posString]){
                    if (posThick[posString]) {
                        posThick[posString] += 1
                    } else {
                        posThick[posString] = 1
                    }
                }   
                visited[posString] = true;
            })
        }

        for (var i=0; i< aggregates.length; i++) {
            var trajObj = aggregates[i];
            var name = Object.keys(trajObj)[0];
            var traj = trajObj[name];
            context.strokeStyle = this.getColor(i, aggregates.length); //"red";
            
            var lastPos; 
            traj.forEach(function(pos, j) {
                context.lineWidth = posThick[JSON.stringify(pos)]/2; 
                context.beginPath();
                
                let centerX = pos[1]*tileSize + tileSize/2;
                let centerY = pos[0]*tileSize + tileSize/2;
                if (j > 0) {
                    context.moveTo(lastPos[0], lastPos[1]); 
                    context.lineTo(centerX,centerY);
                }
                lastPos = [centerX, centerY];
                context.stroke();  
            })
        }

        for (var stringPos in posThick) {
            let pos = JSON.parse(stringPos);
            let centerX = pos[1]*tileSize + tileSize/2;
            let centerY = pos[0]*tileSize + tileSize/2;
            context.beginPath();
            context.arc(centerX, centerY, 
                tileSize*0.2*(2*posThick[JSON.stringify(pos)])/maxPos, 0, 2*Math.PI);
            context.fillStyle = 'rgba(0,0,0,' +posThick[JSON.stringify(pos)]/maxPos + ')';
            context.fill();
            context.stroke();  
        }
    }

    updateCanvas() {

        var renderAggregates = this.props.aggregates && this.props.aggregates.length > 0;
        if (this.props.map) {
            this.renderMap();

            if (this.props.showPath && !renderAggregates) {
                this.renderPath();
            }
            
            if (renderAggregates) {
                this.renderAggregates();
            }

            if (this.props.pos && !renderAggregates) {
                this.renderAgent();
            }

            if (renderAggregates) {
                this.renderAggregates();
            }
        };
    }

    render() {
        let { bgname, fgname, width, height } = this.props;
        return (
            <div>
                <canvas ref={bgname} width={width} height={height} onMouseMove={this.onMouseMove}/>
                {/* <canvas className={"agentcanvas canvas"} ref={fgname} width={width} height={height} /> */}
            </div>
        )
    }

}

export default CanvasGridworld;