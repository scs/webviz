import React, { PureComponent } from 'react';


class KeyBoardVis extends PureComponent {

    render() {
        var {directionProbs} = this.props;

        return (
            <div className={"arrowBox"}>
                <div className={"arrowItem"}></div>
                <div className={"arrowItem"}>&uarr; {parseFloat(directionProbs["Up"]).toFixed(3)}</div>
                <div className={"arrowItem"}></div>
                <div className={"arrowItem"}>&larr; {parseFloat(directionProbs["Left"]).toFixed(3)}</div>
                <div className={"arrowItem"}>&darr; {parseFloat(directionProbs["Down"]).toFixed(3)}</div>
                <div className={"arrowItem"}>&rarr; {parseFloat(directionProbs["Right"]).toFixed(3)}</div>
            </div>
        )

    }
}

export default class ActionPrediction extends PureComponent {

    render() {
        var { actionPredictions } = this.props;

        var elements = [];
        for (var key in actionPredictions) {
            elements.push(<div>
                {key}:<KeyBoardVis directionProbs={actionPredictions[key]}/>
            </div>)
        }

        return (
            <div className={"flex"}>
                {elements}
            </div>
        )
    }

}