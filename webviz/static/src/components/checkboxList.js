import React, { Component } from 'react';

class CheckboxList extends Component {
    constructor (props, context) {
        super(props, context);

        let checkState = {};

        for (var i=0; i<props.boxes.length; i++) {
            checkState[props.boxes[i]] = false;
        }

        this.state = {
            checkState: checkState
        }
        this.updateCheckState = this.updateCheckState.bind(this);
    }


    updateCheckState(e) {
        let checkName = e.target.id.slice(0,-5);
        let newCheckState = Object.assign({}, this.state.checkState);
        newCheckState[checkName] = !newCheckState[checkName];

        this.setState({
            checkState: newCheckState
        })

        this.props.callback(checkName, newCheckState[checkName]);
    }

    render() {

        let checkNames = this.props.boxes;
        let checkBoxes = checkNames.map(el => {
            return (
                <div>
                    {el}
                    <input id={el + "Check"} type="checkbox" defaultChecked={this.state.checkState[el]} checked={this.state.checkState[el]} onChange={this.updateCheckState} />
                </div>
            )
        })
                
        return(
                <div className={"flex"}>
                    {checkBoxes}
                </div>
        )
    }

}

export default CheckboxList;