#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 21 14:11:23 2018

@author: jpoeppel
"""
try:
    import eventlet
    eventlet.monkey_patch()
except ImportError:
    pass

from flask import Flask
from flask_socketio import SocketIO

socketio = SocketIO(engineio_logger=True)


from . import io
connection_manager = io.ConnectionManager()


def create_app(test_config=None, debug=True):
    """
        Using the application factory pattern.
    """

    app = Flask(__name__, 
                static_folder="static/build/static", 
                template_folder="static/build/"
                )
    app.config.from_object('config') #Will load config.py from root directory
    app.debug = debug

    connection_manager.init_app(app)
    from .main import main as main_blueprint
    app.register_blueprint(main_blueprint)

    socketio.init_app(app, cors_allowed_origins="*") #Prevent problems socket.io connecting to the flask server

    return app