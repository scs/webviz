# -*- coding: utf-8 -*-
"""
Created on Sat Jun 24 00:00:38 2017
Abstracts multiple connection possibilities away.
@author: Jan
"""

try:
    import ipaaca
    ipaaca_available = True
except (ImportError, SyntaxError):
    ipaaca_available = False
    
try:
    import rsb
    rsb_available = True
except (ImportError, SyntaxError):
    rsb_available = False

try:
    from eventlet.green import socket
    eventlet_available = True
except (ImportError): 
    eventlet_available = False
    import socket

try:
    from eventlet.green import threading
except (ImportError): 
    import threading

try:
    if eventlet_available:
        import eventlet.green.zmq as zmq
    else:
        import zmq
    zmq_available = True
except (ImportError):
    zmq_available = False



from . import socketio

try:
    import SocketServer as socketserver
except:
    import socketserver    

import json

app = None

MAX_CONNECTIONS = 5

class Connection(object):
    pass
    
class IpaacaConnection(Connection):
    
    def __init__(self, callback, channel):
        self.inputBuffer = ipaaca.InputBuffer("Ipaaca_Plot")
        self.inputBuffer.register_handler(callback)
        #TODO do not create new buffers for each new category/channel
        self.inputBuffer.add_category_interests(channel)
        
        
class ThreadedTCPServer(socketserver.ThreadingMixIn, socketserver.TCPServer):
    pass
        

class SocketConnection(Connection):
    
    def __init__(self, handler, port):
        
        serverName = "localhost" #socket.gethostname() if we need to allow different machines!
        app.logger.info("Setting up TCP server")
        self.server = ThreadedTCPServer((serverName, port), handler)
        
        self.server_thread = threading.Thread(target=self.server.serve_forever)
        # Exit the server thread when the main thread terminates
        self.server_thread.daemon = True
        self.server_thread.start()
        app.logger.info("Finished setting up tcp server")

    def __del__(self):
        self.server.shutdown()
        self.server.server_close()
        
    
class RSBConnection(Connection):
    
    def __init__(self, callback, channel):
        self.listener = rsb.createListener(channel)
        self.listener.addHandler(callback)
        
    def __del__(self):
        self.listener.deactivate()
        del self.listener

class ZMQRouter(object):
    def __init__(self, handler, port, protocol="tcp"):
        # serverName = "localhost"
        serverName = "127.0.0.1"

        self.clients = []

        self.port = port
        app.logger.info("Setting up ZMQ server")
        self.handler = handler
        self.context = zmq.Context()
        self.socket = self.context.socket(zmq.ROUTER)
        self.socket.bind("{}://{}:{}".format(protocol, serverName, port))
        self.server_thread = threading.Thread(target=self.serve_forever)
        self.server_thread.daemon = True
        self.running = True
        self.server_thread.start()

        self.ident = None


    def serve_forever(self):
        while self.running:
            socketio.sleep(0.001)
            ident, msg = self.socket.recv_multipart()
            self.clients.append(ident)
            self.ident = ident


            # message = self.socket.recv_json()
            # self.socket.send_json("acknowledged")
            message = json.loads(msg.decode('unicode_escape').strip('"'))
            # message = json.loads(msg.strip('"'))
            app.logger.info("received data: {}, loaded type: {}".format(message, type(message)))
            message["connection"] = "zmq:{}".format(self.port)
            self.handler(message)


    def send(self, msg):
        app.logger.info("sending message {} to {}".format(msg, self.ident))
        if self.ident:
            self.socket.send_multipart([self.ident, msg.encode('utf-8')])
        else:
            app.logger.info("No ident set to send message to")

    def __del__(self):
        self.running = False
        self.socket.close()
        self.context.term()
        
        
class ConnectionManager(object):
    
    def __init__(self, app_inst=None):
        global app 
        app = app_inst
        self.connections = {}

    def init_app(self, app_inst):
        global app 
        app = app_inst
        self.app = app
         
    def add_connection(self, channel, callback, protocol):
        channelID = protocol + ":" + channel
        app.logger.info("trying to add {}".format(channelID))
        if channelID in self.connections:
            #Connection already established, do nothing
            app.logger.info("channelID {} already included".format(channelID))
            return
        if protocol == "rsb" and rsb_available: 
            self.connections[channelID] = RSBConnection(callback, "/" + channel)
        elif protocol == "ipaaca" and ipaaca_available:
            self.connections[channelID] = IpaacaConnection(callback, channel)
        elif protocol == "tcp":
            #Use port as channel in the tcp case for now
            #The callback needs to be a HandlerClass!
            self.connections[channelID] = SocketConnection(callback, int(channel))
        elif protocol == "zmq" and zmq_available:
            self.connections[channelID] = ZMQRouter(callback, int(channel))
        else:
            app.logger.error("Could not add connection for protocol {}. Is this \
                             protocol available?".format(protocol))
            
    def remove_connection(self, channel):
        app.logger.info("removing connection for channel: {}".format(channel))
        app.logger.info("Currently contained channels: {}".format(self.connections.keys()))
        try:
            del self.connections[channel]
        except KeyError:
            app.logger.debug("Key error when removing channel: {}".format(channel))
            pass

    def notify(self, channel, msg):
        if channel in self.connections:
            self.connections[channel].send(msg)
        else:
            app.logger.debug("Cannot notify channel {}. Channel unknown".format(channel))
        
    def clear(self):
        for con in list(self.connections.keys()):
            del self.connections[con]


    def __del__(self):
        app.logger.info("deconstructing connectionManager")
        self.clear()
        
    
    
if __name__ == "__main__":
    
    def dummyHandler(event):
        js = json.loads(event.data)
        print("Event: ", js)
    con = RSBConnection(dummyHandler, "/test")
    import time
    while True:
        time.sleep(0.1)